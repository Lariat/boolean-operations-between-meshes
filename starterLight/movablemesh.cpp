#include "movablemesh.h"
#include "moller.h"
#include "earcut.hpp"

//#define EPSILON (0.0000001)
void
MovableMesh::Init(){
    deplacement[0] = 0;
    deplacement[1] = 0;
    deplacement[2] = 0;
    intersections = new QVector<Edge>[mesh.n_faces()];
    for(int i=0; i<mesh.n_faces(); i++){
        intersections[i] = QVector<Edge>();
    }
    it_pts = std::map<SETPOI, VertexHandle, MMPointCompare>();
}
MovableMesh::MovableMesh(MyMesh _mesh)
    :mesh(_mesh)
{Init();}

MyMesh MovableMesh::getMesh(){
    MyMesh meshh;
    concatToMesh(meshh);
    return meshh;
}

void MovableMesh::displace(MyMesh::Point displacement){
    deplacement += displacement;
}

void MovableMesh::concatToMesh(MyMesh &_m){
    if(!(mesh.n_vertices()>0 || mesh.n_faces()>0)){qDebug() << "Pas assez de vertices pour concat"; return;}
    for (MyMesh::FaceIter curFace = mesh.faces_begin(); curFace != mesh.faces_end(); curFace++){
        FaceHandle fh = *curFace;
        //QVector<VertexHandle> vertices;
        std::vector<VertexHandle> vertices;
        for(MyMesh::FaceVertexIter fvi=mesh.fv_begin(fh); fvi.is_valid(); fvi++){
            MyMesh::Point point = mesh.point(*fvi) + deplacement;
            //vertices.pu sh_back(_m.add_vertex(point));
            VertexHandle vh = _m.add_vertex(point);
            vertices.push_back(vh);
            //_m.data(vh).thickness = 10;
            _m.set_color(vh, MyMesh::Color(0, 0, 0));
        }
        FaceHandle to_color= _m.add_face(vertices);
        _m.set_color(to_color, mesh.color(fh));
        for (MyMesh::FaceEdgeIter curEdge = _m.fe_begin(to_color); curEdge != _m.fe_end(to_color); curEdge++)
        {
            _m.data(*curEdge).thickness = lineThickness;
            _m.set_color(*curEdge, MyMesh::Color(0, 0, 0));
        }
    }
}

bool operator==(const MovableMesh& m1, const MovableMesh& m2){
    return m1.mesh.n_vertices() == m2.mesh.n_vertices();
}

void MovableMesh::addIntersetion(int faceIdx, QPair<MyMesh::Point, MyMesh::Point> pair){
    //qDebug() << "ENTREE";
    std::pair<SETPOI, bool> a1 = intersection_points.insert(pair.first);
    std::pair<SETPOI, bool> a2 = intersection_points.insert(pair.second);
    if(it_pts.find(a1.first) == it_pts.end()){
        MyMesh::Point p = *(a1.first) - deplacement;
        VertexHandle vh = mesh.add_vertex(p);
        qDebug() << "Ajout nouveaux sommet" << p[0] << p[1] << p[2];
        it_pts.insert({a1.first, vh});
    }
    if(it_pts.find(a2.first) == it_pts.end()){
        MyMesh::Point p = *(a2.first) - deplacement;
        VertexHandle vh = mesh.add_vertex(p);
        qDebug() << "Ajout nouveaux sommet" << p[0] << p[1] << p[2];
        it_pts.insert({a2.first, vh});
    }
    intersections[faceIdx].push_back({a1.first, a2.first});
}

MovableMesh* MovableMesh::intersectFaceWithFace(QVector<FaceHandle> faces1, MovableMesh &Mmesh2, QVector<FaceHandle> faces2){
    MyMesh m1 = this->getMesh();
    MyMesh m2 = Mmesh2.getMesh();
    int coplanar=0;
    MyMesh intermesh;
    for(FaceHandle f1: faces1) {
        for(FaceHandle f2: faces2) {
            int face1Idx = f1.idx(),
                face2Idx = f2.idx();
            int coplanar=0;
            QPair<bool, QPair<MyMesh::Point, MyMesh::Point>> Z =
                    tri_tri_intersect_with_isectline(m1, face1Idx, m2, face2Idx, &coplanar);
            if(!Z.first)continue;
            this->addIntersetion(face1Idx, Z.second);
            Mmesh2.addIntersetion(face2Idx, Z.second);

            VertexHandle vh1 = intermesh.add_vertex(Z.second.first);
            VertexHandle vh2 = intermesh.add_vertex(Z.second.second);
            VertexHandle vh3 = intermesh.add_vertex(Z.second.second);
            FaceHandle fh = intermesh.add_face(vh1, vh2, vh3);
        }
    }
    return new MovableMesh(intermesh);
}

MovableMesh* MovableMesh::intersectWith(MovableMesh * Mmesh2){
    MyMesh mesh1 = this->getMesh();
    MyMesh mesh2 = Mmesh2->getMesh();
    int coplanar=0;
    MyMesh intermesh;
    for(int face1Idx=0; face1Idx<mesh1.n_faces(); face1Idx++){
    for(int face2Idx=0; face2Idx<mesh2.n_faces(); face2Idx++){

        QPair<bool, QPair<MyMesh::Point, MyMesh::Point>> Z =
                tri_tri_intersect_with_isectline(mesh1, face1Idx, mesh2, face2Idx, &coplanar);
        if(!Z.first || coplanar)continue;
        this->addIntersetion(face1Idx, Z.second);
        Mmesh2->addIntersetion(face2Idx, Z.second);

        VertexHandle vh1 = intermesh.add_vertex(Z.second.first);
        VertexHandle vh2 = intermesh.add_vertex(Z.second.second);
        VertexHandle vh3 = intermesh.add_vertex(Z.second.second);
        FaceHandle fh = intermesh.add_face(vh1, vh2, vh3);

    }}
    MovableMesh * mm = new MovableMesh(intermesh);
    mm->lineThickness=25;
    return mm;
}


QVector<SETPOI> MovableMesh::findNeighbor(int facecID, SETPOI n){
    QVector<SETPOI> neighs;
    for(Edge p : intersections[facecID]){
        if(p.A == n || p.B == n){
            neighs.push_back((p.A==n)?p.B:p.A);
        }
    }
    return neighs;
}

QVector<Edge> MovableMesh::findNeighborEdges(int facecID, SETPOI n){
    QVector<Edge> neighs;
    for(Edge p : intersections[facecID]){
        if(p.A == n){
            neighs.append(p);
        }else if(p.B == n){
            neighs.append({p.B,p.A});
        }
    }
    return neighs;
}

void MovableMesh::add_border_edge(int faceId){
    MyMesh mesh2 = getMesh();
    MyMesh::Point T[3];
    VertexHandle v[3];
    FaceHandle fh1 = mesh2.face_handle(faceId);
    int i=0;
    for(MyMesh::FaceVertexIter fvi=mesh2.fv_begin(fh1); fvi.is_valid(); fvi++){
        T[i] = mesh2.point(*fvi);
        v[i] = *fvi;
        i++;
    }

    double AB1l = (T[1]-T[0]).norm();
    double BC1l = (T[2]-T[1]).norm();
    double CA1l = (T[0]-T[2]).norm();

    QList<SETPOI> onBorder[3];
    std::set<SETPOI, MMPointCompare> allIntersectionsVertices;
    for(Edge e : intersections[faceId]){
        allIntersectionsVertices.insert(e.A);
        allIntersectionsVertices.insert(e.B);
    }
    for(SETPOI s : allIntersectionsVertices){
        MyMesh::Point p = *s;
        double AP1 = (p-T[0]).norm();
        double BP1 = (p-T[1]).norm();
        double CP1 = (p-T[2]).norm();

        //On cherche les points sur les aretes.
             if(abs(AB1l - (AP1+BP1)) < EPSILON){onBorder[0].append(s);}
        else if(abs(BC1l - (BP1+CP1)) < EPSILON){onBorder[1].append(s);}
        else if(abs(CA1l - (AP1+CP1)) < EPSILON){onBorder[2].append(s);}
    }

    std::pair<SETPOI, bool> tts[3] = {intersection_points.insert(T[0]),
                                      intersection_points.insert(T[1]),
                                      intersection_points.insert(T[2])};
    for(int i=0; i<3; i++){
        it_pts.insert({tts[i].first, v[i]});
    }
    for(int i=0; i<3; i++){onBorder[i].append(tts[(i+1)%3].first);}


    //On trie les points sur les lignes pour obtenir
    #pragma omp parallel for
    for(int i=0; i<3; i++){
        std::sort(onBorder[i].begin(), onBorder[i].end(),
                  [&](const SETPOI& m1, const SETPOI& m2){
                     return ((*m1)-T[i]).norm() < ((*m2)-T[i]).norm();
                  });
    }

    for(int i=0; i<3; i++){
        SETPOI pre = tts[i].first;
        for(SETPOI p:onBorder[i]){
            intersections->push_back({pre,p});
            pre = p;
        }
    }

}

std::vector<std::vector<SETPOI>>
MovableMesh::find_cycle(int faceID, SETPOI current, SETPOI pre, std::vector<SETPOI>& already_visited){
    QVector<SETPOI> neighs = findNeighbor(faceID, current);
    std::vector<SETPOI> visited = already_visited;
    visited.push_back(current);

    std::vector<std::vector<SETPOI>> cycles;

    for(SETPOI v : neighs){if(v == pre || v == current){continue;}
        bool find_a_cyle=false;
        for(std::vector<SETPOI>::iterator it = visited.begin(); it!=visited.end(); it++){
            SETPOI visited_vector = *it;
            if(visited_vector == v){
                std::vector<SETPOI> n;
                n.insert(n.end(), it, visited.end());
                cycles.push_back(n);
                find_a_cyle = true;
                break;
            }
        }if(find_a_cyle){continue;}
        std::vector<std::vector<SETPOI>> already_find_cycle = find_cycle(faceID, v, current, visited);
        for(std::vector<SETPOI> c: already_find_cycle){
            cycles.push_back(c);
        }

    }
    return cycles;
}

typedef std::pair<MyMesh::Point, MyMesh::Point> BoundBox;
bool
firstIsInsideSecond(BoundBox A, BoundBox B){
    return B.first[0] <= A.first[0] &&
           B.first[1] <= A.first[1] &&
           A.second[0] <= B.second[0] &&
           A.second[1] <= B.second[1];
}

std::vector<std::vector<SETPOI>>
remove_long_loop(std::vector<std::vector<SETPOI>> & cycles, Projection pro){
    sort(cycles.begin(), cycles.end(),
        [&](const std::vector<SETPOI> &A, const std::vector<SETPOI> &B){
            return A.size() < B.size();
        });

    std::vector<BoundBox> boundboxes;
    for(std::vector<SETPOI> c : cycles){
        BoundBox b = {pro.computeProjection(*(c[0])), pro.computeProjection(*(c[0]))};
        for(SETPOI p: c){
            MyMesh::Point pp = pro.computeProjection(*p);
            if(pp < b.first) {b.first  = pp;}
            if(b.second < pp){b.second = pp;}
        }
        boundboxes.push_back(b);
    }

    std::set<int> to_remove;

    for(int i=0; i< boundboxes.size()-1; i++){
        for(int j=i+1; j< boundboxes.size(); j++){
            if(firstIsInsideSecond(boundboxes[i],
                                   boundboxes[j])){
                to_remove.insert(j);
            }else if(firstIsInsideSecond(boundboxes[i],
                                   boundboxes[j])){
                to_remove.insert(i);
            }
        }
    }

    std::vector<std::vector<SETPOI>> ncycles;
    for(int i=0; i<cycles.size(); i++){
        bool rm=false;
        for(int trm:to_remove){
            if(i == trm){
                rm = true;
                break;
            }
        }if(rm){continue;}
        ncycles.push_back(cycles[i]);
    }
    qDebug() << "";
    return ncycles;
}

QPair<std::vector<Polygone>,std::vector<Polygone>>
MovableMesh::find_all_polygon(int faceID){
    MyMesh mesh2 = getMesh();
    Projection pro(mesh2, mesh2.face_handle(faceID));
    //Stocke tous les sommets d'une face pour vérifier que l'on a créer un polygone avec toutes les faces
    std::vector<SETPOI> vertices;
    for(Edge e: intersections[faceID]){
        vertices.push_back(e.A);vertices.push_back(e.B);
    }

    std::vector<SETPOI> already_visited;
    std::vector<Polygone> polygons;

    //Cela permet d'avoir un point de départ sur un bord du triangle
    SETPOI start;
    for(MyMesh::FaceVertexIter fvi=mesh2.fv_begin(mesh2.face_handle(faceID)); fvi.is_valid(); fvi++){
        MyMesh::Point p2 = mesh2.point((*fvi) );
        start = intersection_points.insert( p2 ).first;
        break;
    }
    std::vector<std::vector<SETPOI>> cycle = find_cycle(faceID, start, start, already_visited);
    qDebug() << "Cycle_size" << cycle.size();
    if(cycle.size() == 0){
        qDebug() << "Problème de loop";
        qDebug() << (*start)[0] << (*start)[1] << (*start)[2];
        qDebug() << "EDGES";
        QDebug qd = qDebug();
        for(Edge e:intersections[faceID]){
            qd << "("<<(*e.A)[0] << (*e.A)[1] << (*e.A)[2]<<")";
            qd << "("<<(*e.B)[0] << (*e.B)[1] << (*e.B)[2]<<")";
        }
        qDebug() << "ALREADYU";
        for(SETPOI s : already_visited){
            qd << "("<<(*s)[0] << (*s)[1] << (*s)[2]<<")";
        }
        std::exit(5);
    }
    cycle = remove_long_loop(cycle, pro);
    for(std::vector<SETPOI> c: cycle){
        polygons.push_back(Polygone(c, pro));}

    std::vector<Polygone> allholes;
    /*Vérifie que tous les sommets ont été visités une fois,
      Sachant que l'on a commencer depuis un sommets du bord,
      les seuls sommets manquant sont ceux des trous
    */
    for(SETPOI v1:vertices){
        bool is_contained = false;
        for(SETPOI v2:already_visited){
            if(v1 == v2){
                is_contained = true;
                break;
            }
        }
        if(!is_contained){
            std::vector<std::vector<SETPOI>> holes = find_cycle(faceID, v1, v1, already_visited);
            holes = remove_long_loop(holes, pro);
            if(holes.size()>0){
            for(std::vector<SETPOI> a:holes){
               if(a.size() > 0){
                    allholes.push_back(Polygone(a,pro));}
            }}
        }
    }
    return {polygons, allholes};
}

QVector<VFace>
MovableMesh::triangulateOperation(int faceID){
    qDebug() << (*intersections[faceID][0].A)[0] << (*intersections[faceID][0].A)[1] << (*intersections[faceID][0].A)[2];
    qDebug() << (*intersections[faceID][0].B)[0] << (*intersections[faceID][0].B)[1] << (*intersections[faceID][0].B)[2];
    add_border_edge(faceID);
    QPair<std::vector<Polygone>,std::vector<Polygone>> polys_holes = find_all_polygon(faceID);
    qDebug() << polys_holes.first.size();

    QVector<VFace> ops;
    for(Polygone p : polys_holes.first){
        std::vector<SETPOI> faces = p.triangulate();
        if(faces.size() == 0){
            qDebug() << "LErrue";
            std::exit(6);
        }
        if(faces.size()%3 != 0){qDebug() << "Erreur mod3" << faces.size(); std::exit(1);}
        for(int i=0; i<faces.size();){
            VFace op;
            for(int j=0; j<3; j++){
                op[j] = (*it_pts.find(faces[i])).second;
                i++;
            }
            ops.push_back(op);
        }
    }
    return ops;
}

void MovableMesh::generateLoop(){
    qDebug() << "NB_POINT_INTERSECTION" << intersection_points.size();
    MyMesh mesh2;
    for(MyMesh::VertexIter vi=mesh.vertices_begin(); vi!= mesh.vertices_end(); vi++){
        MyMesh::Point p = mesh.point(*vi);
        mesh2.add_vertex(p);
    }
    for(int i=0; i<mesh.n_faces(); i++){
        //Decommenter pour tester l'itersection entre objet (ne fonctionne qu'avec TEST et no_secant_tri2.obj )
        //Ne fonctionne que si  no_secant_tri2.obj créer une arete de bord à bord sur TEST
        /*if(intersections[i].size() == 0){
            std::vector<VertexHandle> vhs;
            for(MyMesh::FaceVertexIter fvi=mesh.fv_begin(mesh.face_handle(i)); fvi.is_valid(); fvi++){
                vhs.push_back(*fvi);
            }
            mesh2.add_face(vhs);
            continue;
        }

        QVector<VFace> opstriangulateOperation(i);

        for(VFace op : ops){
            mesh2.add_face({op[0], op[1], op[2]});
        }//*/
    }
    //mesh = mesh2;
}
