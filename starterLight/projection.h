#ifndef PROJECTION_H
#define PROJECTION_H

#include "mymesh.h"
#include <QVector3D>
#include <QDebug>

class Projection
{
public:
    MyMesh::Point offset_to_origin;

    //represente les vecteurs de notre base non-normalisés
    MyMesh::Point ab, ac;

    //represente les vecteurs de notre base normalisés
    MyMesh::Point ab_n, ac_n;

    Projection(MyMesh mesh, FaceHandle tri);
    //le z est a 0 ce sont des point 2D ui sont retournés
    MyMesh::Point computeProjection(MyMesh::Point pt);

};

#endif // PROJECTION_H
