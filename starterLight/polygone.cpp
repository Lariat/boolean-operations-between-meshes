#include "polygone.h"
#include "earcut.hpp"

Polygone::Polygone(std::vector<SETPOI> _points, Projection pro)
{
    points = std::vector<std::pair<SETPOI, TPoint>>();
    MyMesh::Point center(0,0,0);
    for(SETPOI s: _points){
        MyMesh::Point p = pro.computeProjection(*s);
        center += p;
        points.push_back({s, {p[0], p[1]}});
    }
    center /= _points.size();
    std::sort(points.begin(), points.end(),
        [&](const std::pair<SETPOI, TPoint> &A, const std::pair<SETPOI, TPoint> &B){
            return atan2(A.second[1]-center[1], A.second[0]-center[0]) > atan2(B.second[1]-center[1], B.second[0]-center[0]);
        });
}

std::vector<SETPOI> Polygone::triangulate(){
    TPoint A = {0,0}; TPoint B = {0,1}; TPoint C = {1,0};
    std::vector<TPoint> plm; plm.push_back(A); plm.push_back(B); plm.push_back(C);
    std::vector<std::vector<TPoint>> pmo; pmo.push_back(plm);
    qDebug() << mapbox::earcut<uint32_t>(pmo).size();

    qDebug() << "Nombre polygone" << points.size();
    std::vector<TPoint> cycle;
    for(std::pair<SETPOI, TPoint> p: points){
        cycle.push_back(p.second);
        qDebug() << p.second[0] << p.second[1];
    }
    std::vector<std::vector<TPoint>> polygon;
    polygon.push_back(cycle);
    std::vector<uint32_t> indices = mapbox::earcut<uint32_t>(polygon);
    qDebug() << indices.size();
    std::vector<SETPOI> triangulation;
    for(uint32_t i:indices){
        triangulation.push_back(points[i].first);
    }

    return triangulation;
}



QString Polygone::toString(){
    QString qs("Polygone(");
    for(std::pair<SETPOI, TPoint> aze : points){
        qs.append(QString("( %1, %2 )").arg(aze.second[0]).arg(aze.second[1]));
    }
    qs.append(")");
    return qs;
}
