#include "projection.h"
#include <algorithm>
#include <limits>

Projection::Projection(MyMesh mesh, FaceHandle tri)
{
    std::array<MyMesh::Point, 3> tri_vertices;
    int i = 0;
    for(MyMesh::FaceVertexIter fv_iter = mesh.fv_iter(tri); fv_iter.is_valid(); fv_iter++ ) {
        VertexHandle vh = *fv_iter;
        MyMesh::Point pt = mesh.point(vh);
        tri_vertices[i] = pt;
        i++;
    }
    offset_to_origin = tri_vertices[0];

    //tri_vertices[0] -= offset_to_origin;
    tri_vertices[1] -= offset_to_origin;
    tri_vertices[2] -= offset_to_origin;

    ab = tri_vertices[1];
    ac = tri_vertices[2];
    ab_n = tri_vertices[1];
    ac_n = tri_vertices[2];
    ab_n.normalize();
    ac_n.normalize();
}

bool AreSame(float a, float b) {
    return std::fabs(a - b) < std::numeric_limits<float>::epsilon();
}

MyMesh::Point Projection::computeProjection(MyMesh::Point pt)
{
    MyMesh::Point res;

    /*qDebug() << "ab : \n"
             << ab [0] << ab[1] << ab[2];

    qDebug() << "ac : \n"
             << ac[0] << ac[1] << ac[2];*/

    pt -= offset_to_origin;
    if(AreSame(pt.norm(), 0.0f)) {
        return pt;
    }

    //composante de pt dans la direction ab
    //projection scalaire
    float comp_pt_ab = (pt | ab_n);
    float lambda = comp_pt_ab/(ab.norm());

    //composante de pt dans la direction ac
    //projection scalaire
    float comp_pt_ac = (pt | ac_n);
    float mu = comp_pt_ac/(ac.norm());

    /*qDebug() << "Lambda : " << lambda << "\n"
             << "Mu : " << mu ;*/

    if(AreSame(lambda, 1.0)) {
        mu = 0.0f;
    }
    else if(AreSame(mu, 1.0)) {
        lambda = 0.0f;
    }
    /*else {
        qDebug() << "imaginary just like the justice : ";
    }*/

    res[0] = lambda;
    res[1] = mu;
    res[2] = 0.0f;
    //res = res/res.norm();

    /*qDebug() << "Point à projeter : (post décallage à l'origine)"
             << pt[0] << pt[1] << pt[2];*/

    MyMesh::Point test = (res[0] * ab) + (res[1] * ac);
    //test += offset_to_origin;
    /*qDebug() << "Retombons-nous sur nos pattes ?"
             << test[0] << test[1] << test[2];*/


    return res;
}
