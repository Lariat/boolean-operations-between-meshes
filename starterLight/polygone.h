#ifndef POLYGONE_H
#define POLYGONE_H
#include "mymesh.h"
#include "projection.h"

typedef std::array<float, 2> TPoint;

class Polygone
{
    std::vector<std::pair<SETPOI, TPoint>> points;
public:
    Polygone(std::vector<SETPOI> points, Projection pro);
    Polygone(){}
    std::vector<SETPOI> triangulate();


    QString toString();
};

#endif // POLYGONE_H
