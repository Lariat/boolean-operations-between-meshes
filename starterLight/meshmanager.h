#ifndef MESHMANAGER_H
#define MESHMANAGER_H

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <QVector>
class meshManager
{
    struct MyTraits : public OpenMesh::DefaultTraits
    {
        // use vertex normals and vertex colors
        VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color | OpenMesh::Attributes::Status);
        // store the previous halfedge
        HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
        // use face normals face colors
        FaceAttributes( OpenMesh::Attributes::HIDDEN | OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color | OpenMesh::Attributes::Status);
        EdgeAttributes( OpenMesh::Attributes::Color | OpenMesh::Attributes::Status );
        // vertex thickness
        VertexTraits{float thickness; float value;};
        // edge thickness
        EdgeTraits{float thickness;};
    };
    typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;
public:
    meshManager();
    QVector<MyMesh>meshes;
};

#endif // MESHMANAGER_H
