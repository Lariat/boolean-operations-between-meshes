#include <QDebug>
#include <QVector3D>
#include "mymesh.h"
#include "point3d.h"

//Une Bound box est un cube qui englobe un mesh
class BoundBox
{
public:
    //la bound box se crée dans le constructeur
    BoundBox(MyMesh mesh, MyMesh::Point offset);
    BoundBox();

    bool contains(MyMesh mesh, MyMesh::Point offset);
    bool contains_strictly(BoundBox& other);
    bool contains(BoundBox& other);
    bool contains(MyMesh::Point &pt);
    bool contains(MyMesh mesh, MyMesh::Point offset, FaceHandle f);
    float volume();
    float min_volume(int depth);


    //@TODO : creer une BoundBox qui engloble les deux premiers meshs
    bool overlap(BoundBox& other, BoundBox* out);
    //@TODO : partionne la double BoundBox en 8 -> crée un Octree
    std::array<BoundBox, 8> partition();

    //Optionnel -> méthode pour dessiner une Boundbox et les octrees succesifs
    //std::array<std::array<float, 72>, 2> draw();

    //Comme on peut déplacer un mesh sa boundbox doit suivre
    MyMesh::Point offset;
    //deux sommets pour représenter la bounding box
    MyMesh::Point min;
    MyMesh::Point max;
    void draw(MyMesh &_mesh, MyMesh::Point offset, MyMesh::Color col){
        int SiZe = 4;

        VertexHandle vh1 = _mesh.add_vertex(offset + MyMesh::Point (min[0],min[1],min[2]));
        _mesh.data(vh1).thickness = SiZe;  _mesh.set_color(vh1, col);

        VertexHandle vh = _mesh.add_vertex(offset + MyMesh::Point (min[0],max[1],min[2]));
        _mesh.data(vh).thickness = SiZe;  _mesh.set_color(vh, col);

        vh = _mesh.add_vertex(offset + MyMesh::Point(max[0],min[1],min[2]));
        _mesh.data(vh).thickness = SiZe;  _mesh.set_color(vh, col);

        vh = _mesh.add_vertex(offset + MyMesh::Point(max[0],max[1],min[2]));
        _mesh.data(vh).thickness = SiZe;  _mesh.set_color(vh, col);

        vh = _mesh.add_vertex(offset + MyMesh::Point(min[0],min[1],max[2]));
        _mesh.data(vh).thickness = SiZe;  _mesh.set_color(vh, col);

        vh = _mesh.add_vertex(offset + MyMesh::Point(min[0],max[1],max[2]));
        _mesh.data(vh).thickness = SiZe;  _mesh.set_color(vh, col);

        vh = _mesh.add_vertex(offset + MyMesh::Point(max[0],min[1],max[2]));
        _mesh.data(vh).thickness = SiZe;  _mesh.set_color(vh, col);

        vh = _mesh.add_vertex(offset + MyMesh::Point(max[0],max[1],max[2]));
        _mesh.data(vh).thickness = SiZe;  _mesh.set_color(vh, col);
    }
};

