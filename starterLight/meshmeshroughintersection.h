#ifndef MESHMESHROUGHINTERSECTION_H
#define MESHMESHROUGHINTERSECTION_H

#include "octtree.h"

class MeshMeshRoughIntersection
{
public:
    void get_intersection_region(OctTree oct1, OctTree oct2, std::vector<OctTree> &res);
};

#endif // MESHMESHROUGHINTERSECTION_H
