#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tri_intersection_tools.h"
#include <QProgressBar>

/* **** début de la partie boutons et IHM **** */

void MainWindow::Dodavork_intersection(){

}

void MainWindow::on_pushButton_doda_clicked() {

    //mesh_res.clean();
    Dodavork_intersection();
    //mesh_res.update_normals();
    //displayMesh(&mesh_res);
}

void MainWindow::addMesh(QString fileName, MyMesh meshh){
    int iMax=0;
    for(QPair<QListWidgetItem*, MovableMesh*> p:meshList){
        QStringList li = p.first->text().split("/");
        QString fName = li.first();
        if(fName == fileName){
            if(li.size() < 2){
                iMax = std::max(1,iMax);
                continue;
            }
            int fNumb = (p.first->text().split("/").last()).toInt();
            iMax = std::max(fNumb,1);
        }
    }

    if(iMax != 0){fileName.append(QString("/"));fileName.append(iMax);}

    QListWidgetItem * z = new QListWidgetItem(fileName, ui->listMesh);
    meshList.push_back(QPair<QListWidgetItem*, MovableMesh*>(z, new MovableMesh(meshh)));
    ui->listMesh->addItem(z);
    displayMeshes();
}

void MainWindow::addMesh()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.obj)"));
    MyMesh meshh;
    OpenMesh::IO::read_mesh(meshh, fileName.toUtf8().constData());
    resetAllColorsAndThickness(&meshh);
    fileName = fileName.split("/").last().split(".").first();
    addMesh(fileName, meshh);
}

void MainWindow::removeMesh(){
    QList<QListWidgetItem *> items = ui->listMesh->selectedItems();
    for(QListWidgetItem * i : items){
        for(int idx=0; idx<meshList.size(); idx++){
            if(i->text() == meshList[idx].first->text()){
                meshList.remove(idx);
                break;
            }
        }
    }
    for(QListWidgetItem * i:items){
        delete ui->listMesh->takeItem(ui->listMesh->row(i));
    }
    displayMeshes();
}

QVector<MovableMesh*> MainWindow::get_active_mesh(){
    QList<QListWidgetItem *> items = ui->listMesh->selectedItems();
    QVector<MovableMesh*> meshes;
    for(QListWidgetItem * i : items){
        for(int idx=0; idx<meshList.size(); idx++){
            if(i->text() == meshList[idx].first->text()){
                meshes.push_back(meshList[idx].second);
                break;
            }
        }
    }
    return meshes;
}

QVector<std::pair<MovableMesh*, int>> MainWindow::get_active_mesh_plus_index(){
    QList<QListWidgetItem *> items = ui->listMesh->selectedItems();
    QVector<std::pair<MovableMesh*, int>> meshes;
    for(QListWidgetItem * i : items){
        for(int idx=0; idx<meshList.size(); idx++){
            if(i->text() == meshList[idx].first->text()){
                meshes.push_back(std::pair<MovableMesh*, int>(meshList[idx].second, idx));
                break;
            }
        }
    }
    return meshes;
}

void MainWindow::deplacementMesh(MyMesh::Point deplacement){
    QVector<MovableMesh*> meshes = get_active_mesh();
    for(MovableMesh *m : meshes){
        MyMesh mesh = m->getMesh();
        m->displace(deplacement);
    }
    displayMeshes();
}


void MainWindow::on_pushButton_oct_clicked() {

    MyMesh test;
    QProgressBar * pg = new QProgressBar() ;
    ui->spinBox_dDepth->setMaximum(ui->spinBox_depth->value());
    //Si le mesh de base n'existe pas on stop
    int i = 0;
    int size = meshList.size();
    octreeList.clear();
    int tmp = 0;    for(QPair<QListWidgetItem*, MovableMesh*> pair : meshList) {
        qDebug() << "mesh n°" << i << " : " << pair.second->mesh.n_vertices();
        i++;
        if(pair.second->mesh.vertices_empty()) {
            continue;
        }
        BoundBox box(pair.second->mesh, pair.second->getOffset());

        OctTree oct = OctTree(box);

        QVector<FaceHandle> faces;
        for(MyMesh::FaceIter f_it = pair.second->mesh.faces_begin(); f_it != pair.second->mesh.faces_end(); ++f_it) {
            faces.push_back(*f_it);
        }
        tmp = ui->spinBox_depth->value();
        oct.buildTree(pair.second->mesh, pair.second->getOffset(), faces,
                      tmp , volume);

        pair.second->concatToMesh(test);
        octreeList.push_back(oct);
        pg->setValue(i/size);
    }
    ui->spinBox_depth->setValue(tmp - 1);
    ui->spinBox_dDepth->setMaximum(ui->spinBox_depth->value());
    displayMesh(&test);
}

std::pair<QVector<FaceHandle>, QVector<FaceHandle>> MainWindow::find_smallest_region(MovableMesh m_1, MovableMesh m_2, OctTree m_1t, OctTree m_2t) {
    BoundBox r = (m_1t.region);
    BoundBox l = (m_2t.region);
    BoundBox res;
    QVector<MyMesh::FaceHandle> handles_r;
    QVector<MyMesh::FaceHandle> handles_l;
    int depth = ui->spinBox_dDepth->value();
    qDebug() << "Profondeur : " << __FUNCTION__ << depth;

    if(r.overlap(l, &res)) {
        std::vector<OctTree> M1;
        m_1t.Octree_as_sub_array(depth, m_1t, M1);
        std::vector<OctTree> M2;
        m_2t.Octree_as_sub_array(depth, m_2t, M2);
        for(size_t i = 0; i < M1.size(); i++) {
            r = M1.at(i).region;
            for(size_t j = 0; j < M2.size(); j++) {
                l = M2.at(j).region;
                if(r.overlap(l, &res)) {
                    for(FaceHandle fh : M1.at(i).mesh_face) {
                        handles_r.push_back(fh);
                    }
                    for(FaceHandle fh : M2.at(j).mesh_face) {
                        handles_l.push_back(fh);
                    }
                }
            }

        }
    }
    std::pair<QVector<FaceHandle>, QVector<FaceHandle>> handles =
            std::pair<QVector<FaceHandle>, QVector<FaceHandle>>(handles_r, handles_l);
    return handles;
}

void MainWindow::on_spinBox_volume_valueChanged(int v)
{
    volume = (float)v / 10000.0f;
    ui->label_vol->setText(QString("volume min region : %1 ").arg(volume));
}

void MainWindow::on_spinBox_spd_valueChanged(int v)
{
    of = v / 100.0f;
    ui->label_pas->setText(QString("pas de déplacement : %1 ").arg(of));
}

void MainWindow::on_pushButton_Rintersect_clicked()
{
    if(octreeList.size() <= 1) {
        qDebug() << "Octrees non alloués : Abandon !";
        return;
    }
    std::vector<OctTree> result;

    for(int i = 0; i < meshList.size() - 1; i++) {
        MovableMesh *m1 = meshList.at(i).second;
        MovableMesh *m2 = meshList.at(i + 1).second;
        OctTree oct1 = octreeList.at(i);
        OctTree oct2 = octreeList.at(i + 1);

        std::pair<QVector<FaceHandle>, QVector<FaceHandle>> fhs = find_smallest_region(*m1, *m2, oct1, oct2);

        resetAllColorsAndThickness(&m1->mesh);
        resetAllColorsAndThickness(&m2->mesh);

        for(FaceHandle fh : fhs.first) {
            FaceHandle grrr = m1->mesh.face_handle(fh.idx());
            m1->mesh.set_color(grrr, MyMesh::Color(255, 0, 0));
        }
        for(FaceHandle fh : fhs.second) {
            FaceHandle grrr = m2->mesh.face_handle(fh.idx());
            m2->mesh.set_color(grrr, MyMesh::Color(0, 0, 255));
        }
    }
    displayMeshes();
    /*
    int i = 0;
    for(OctTree oct : result) {
        MovableMesh *m = meshList.at(i).second;
        MyMesh associate = m->getMesh();
        oct.draw(associate, m->getOffset(), MyMesh::Color(0,0,255));
    }*/
}

void MainWindow::on_pushButton_Test_clicked()
{
    MyMesh test;
    //Si le mesh de base n'existe pas on stop
    int i = 0;
    for(QPair<QListWidgetItem*, MovableMesh*> pair : meshList) {
        qDebug() << "mesh n°" << i << " : " << pair.second->mesh.n_vertices();

        if(pair.second->mesh.vertices_empty()) {
            continue;
        }
        MyMesh::Point pt = MyMesh::Point(0,0,0);//pair.second->getOffset();

        OctTree cur = octreeList.at(i);
        resetAllColorsAndThickness(&pair.second->mesh);
        cur.draw(test, ui->spinBox_dDepth->value(), pt);

        pair.second->concatToMesh(test);
        i++;
    }
    displayMesh(&test);
}

inline void colorEdge(MyMesh * mesh, FaceHandle fh, MyMesh::Color c){
    for(MyMesh::FaceEdgeIter fei=mesh->fe_begin(fh); fei.is_valid(); fei++){
        mesh->set_color(*fei, c);
    }
}

//Déclenche l'intersection du mesh courant avec les autres.
void MainWindow::on_pushButton_tdi_clicked(){

    MyMesh test;
    QVector<MovableMesh *> meshes = get_active_mesh();
    int i = 0;
    for(MovableMesh * m : meshes){
        MyMesh m1 = m->getMesh();
        for(QPair<QListWidgetItem*, MovableMesh*> pair : meshList){
            if(pair.second == m){qDebug() << pair.first->text(); continue;}
            MovableMesh * mm = m->intersectWith(pair.second);
            if(mm->mesh.n_vertices() == 0)continue;
            mm->concatToMesh(test);
        }
        m->generateLoop();
    }
    addMesh("Intersection", test);


}

void MainWindow::on_pushButton_tdio_clicked()
{
    MyMesh test;
    if(octreeList.size() <=1) {
        qDebug() << "Not enough Octrees, work abandonned";
        return;
    }
    QVector<std::pair<MovableMesh*, int>> meshes = get_active_mesh_plus_index();
    for(std::pair<MovableMesh*, int> m : meshes){
        MovableMesh *m1 = m.first;
        OctTree oct1 = octreeList.at(m.second);
        int i = 0;
        for(QPair<QListWidgetItem*, MovableMesh*> pair : meshList){
            if(i == m.second){
                qDebug() << i << " : " << pair.first->text();
                i++;
                continue;
            }
            MovableMesh *m2 = pair.second;
            OctTree oct2 = octreeList.at(i);
            std::pair<QVector<FaceHandle>, QVector<FaceHandle>> fhs = find_smallest_region(*m1, *m2, oct1, oct2);
            MovableMesh * mm = m1->intersectFaceWithFace(fhs.first, *m2, fhs.second);
            mm->concatToMesh(test);
            i++;
        }
        //m1->generateLoop();
    }
    addMesh("Intersection", test);

}

/* **** fin de la partie boutons et IHM **** */



/* **** fonctions supplémentaires **** */
// permet d'initialiser les couleurs et les épaisseurs des élements du maillage
void MainWindow::resetAllColorsAndThickness(MyMesh* _mesh)
{
    for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
    {
        _mesh->data(*curVert).thickness = 1;
        _mesh->set_color(*curVert, MyMesh::Color(0, 0, 0));
    }

    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        _mesh->set_color(*curFace, MyMesh::Color(150, 150, 150));
    }

    for (MyMesh::EdgeIter curEdge = _mesh->edges_begin(); curEdge != _mesh->edges_end(); curEdge++)
    {
        _mesh->data(*curEdge).thickness = 1;
        _mesh->set_color(*curEdge, MyMesh::Color(0, 0, 0));
    }
}

void MainWindow::displayMeshes(){
    MyMesh mesh;
    for(QPair<QListWidgetItem*, MovableMesh*> p : meshList){
        p.second->concatToMesh(mesh);
        /*for(MyMesh::VertexIter vi=p.second.mesh.vertices_begin(); vi->is_valid(); vi++){
            MyMesh::Point po = p.second.mesh.point(*vi);
            qDebug() << po[0] << po[1] << po[2];
        }*/
    }
    displayMesh(&mesh);
}

// charge un objet MyMesh dans l'environnement OpenGL
void MainWindow::displayMesh(MyMesh* _mesh, bool isTemperatureMap, float mapRange)
{
    GLuint* triIndiceArray = new GLuint[_mesh->n_faces() * 3];
    GLfloat* triCols = new GLfloat[_mesh->n_faces() * 3 * 3];
    GLfloat* triVerts = new GLfloat[_mesh->n_faces() * 3 * 3];

    int i = 0;

    if(isTemperatureMap)
    {
        QVector<float> values;

        if(mapRange == -1)
        {
            for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
                values.append(fabs(_mesh->data(*curVert).value));
            std::sort(values.begin(), values.end());
            mapRange = values.at(values.size()*0.8);
            qDebug() << "mapRange" << mapRange;
        }

        float range = mapRange;
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;

        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }
    else
    {
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;
        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }


    ui->displayWidget->loadMesh(triVerts, triCols, _mesh->n_faces() * 3 * 3, triIndiceArray, _mesh->n_faces() * 3);

    delete[] triIndiceArray;
    delete[] triCols;
    delete[] triVerts;

    GLuint* linesIndiceArray = new GLuint[_mesh->n_edges() * 2];
    GLfloat* linesCols = new GLfloat[_mesh->n_edges() * 2 * 3];
    GLfloat* linesVerts = new GLfloat[_mesh->n_edges() * 2 * 3];

    i = 0;
    QHash<float, QList<int> > edgesIDbyThickness;
    for (MyMesh::EdgeIter eit = _mesh->edges_begin(); eit != _mesh->edges_end(); ++eit)
    {
        float t = _mesh->data(*eit).thickness;
        if(t > 0)
        {
            if(!edgesIDbyThickness.contains(t))
                edgesIDbyThickness[t] = QList<int>();
            edgesIDbyThickness[t].append((*eit).idx());
        }
    }
    QHashIterator<float, QList<int> > it(edgesIDbyThickness);
    QList<QPair<float, int> > edgeSizes;
    while (it.hasNext())
    {
        it.next();

        for(int e = 0; e < it.value().size(); e++)
        {
            int eidx = it.value().at(e);

            MyMesh::VertexHandle vh1 = _mesh->to_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh1)[0];
            linesVerts[3*i+1] = _mesh->point(vh1)[1];
            linesVerts[3*i+2] = _mesh->point(vh1)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;

            MyMesh::VertexHandle vh2 = _mesh->from_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh2)[0];
            linesVerts[3*i+1] = _mesh->point(vh2)[1];
            linesVerts[3*i+2] = _mesh->point(vh2)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;
        }
        edgeSizes.append(qMakePair(it.key(), it.value().size()));
    }

    ui->displayWidget->loadLines(linesVerts, linesCols, i * 3, linesIndiceArray, i, edgeSizes);

    delete[] linesIndiceArray;
    delete[] linesCols;
    delete[] linesVerts;

    GLuint* pointsIndiceArray = new GLuint[_mesh->n_vertices()];
    GLfloat* pointsCols = new GLfloat[_mesh->n_vertices() * 3];
    GLfloat* pointsVerts = new GLfloat[_mesh->n_vertices() * 3];

    i = 0;
    QHash<float, QList<int> > vertsIDbyThickness;
    for (MyMesh::VertexIter vit = _mesh->vertices_begin(); vit != _mesh->vertices_end(); ++vit)
    {
        float t = _mesh->data(*vit).thickness;
        if(t > 0)
        {
            if(!vertsIDbyThickness.contains(t))
                vertsIDbyThickness[t] = QList<int>();
            vertsIDbyThickness[t].append((*vit).idx());
        }
    }
    QHashIterator<float, QList<int> > vitt(vertsIDbyThickness);
    QList<QPair<float, int> > vertsSizes;

    while (vitt.hasNext())
    {
        vitt.next();

        for(int v = 0; v < vitt.value().size(); v++)
        {
            int vidx = vitt.value().at(v);

            pointsVerts[3*i+0] = _mesh->point(_mesh->vertex_handle(vidx))[0];
            pointsVerts[3*i+1] = _mesh->point(_mesh->vertex_handle(vidx))[1];
            pointsVerts[3*i+2] = _mesh->point(_mesh->vertex_handle(vidx))[2];
            pointsCols[3*i+0] = _mesh->color(_mesh->vertex_handle(vidx))[0];
            pointsCols[3*i+1] = _mesh->color(_mesh->vertex_handle(vidx))[1];
            pointsCols[3*i+2] = _mesh->color(_mesh->vertex_handle(vidx))[2];
            pointsIndiceArray[i] = i;
            i++;
        }
        vertsSizes.append(qMakePair(vitt.key(), vitt.value().size()));
    }

    ui->displayWidget->loadPoints(pointsVerts, pointsCols, i * 3, pointsIndiceArray, i, vertsSizes);

    delete[] pointsIndiceArray;
    delete[] pointsCols;
    delete[] pointsVerts;
}

typedef struct{
    MyMesh::Point head, tail;
}EEdge;

typedef struct{
    std::vector<EEdge> edges;
}OrientedLoop;

QVector<MyMesh::Point> m_aVerts;
QVector<EEdge> m_aEdges;
QVector<QVector<int>> m_aLoops;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    MyMesh mesh5_1, mesh5_2, TEST;
    std::vector<VertexHandle> vhs;

    VertexHandle A1 = mesh5_1.add_vertex({+0.00, +0.00, +1.25}),
            B1 = mesh5_1.add_vertex({-1.25, +0.00, +0.00}),
            C1 = mesh5_1.add_vertex({+1.25, +0.00, +0.00}),

            A2 = mesh5_2.add_vertex({-1.00, +1.00, +0.5}),
            B2 = mesh5_2.add_vertex({+1.00, +1.00, +0.5}),
            C2 = mesh5_2.add_vertex({+0.00, -1.00, +0.5});

    vhs = {A1,B1,C1};
    FaceHandle fh = mesh5_1.add_face(vhs);
    Projection pro(mesh5_1, fh);
    VertexHandle vh;
    TEST.add_vertex(mesh5_1.point(A1));
    TEST.add_vertex(mesh5_1.point(B1));
    TEST.add_vertex(mesh5_1.point(C1));
    TEST.add_face({A1, B1, C1});


    vhs = {A2,B2,C2};mesh5_2.add_face(vhs);

    //resetAllColorsAndThickness(&mesh5_1);
    //resetAllColorsAndThickness(&mesh5_2);
    resetAllColorsAndThickness(&TEST);

    MyMesh::Point deb = pro.computeProjection(mesh5_1.point(A1));
    deb = pro.computeProjection(MyMesh::Point(0,0, 0.25));
    qDebug() << "projection DEB : (" << deb[0] << ", "<< deb[1] <<")\n";


    TEST.update_normals();
    addMesh("TEST", TEST);
    //addMesh("Test1", mesh5_1);
    //addMesh("Test2", mesh5_2);

    displayMeshes();
    //displayMesh(&TEST);
}


MainWindow::~MainWindow()
{
    delete ui;
}
