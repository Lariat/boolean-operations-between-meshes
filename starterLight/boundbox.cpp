#include "boundbox.h"

BoundBox::BoundBox(MyMesh mesh, MyMesh::Point offset)
{
    int size = 0;
    //calcul du centre du mesh et des coords min/max

    MyMesh::VertexHandle init = mesh.vertex_handle(0);
    this->offset = offset;

    if(! init.is_valid()) return;

    float min_x, max_x, min_y, max_y, min_z, max_z;
    min_x = max_x = mesh.point(init)[0];
    min_y = max_y = mesh.point(init)[1];
    min_z = max_z = mesh.point(init)[2];

    // iterate over all vertices
    for (MyMesh::VertexIter v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
        MyMesh::Point coord = mesh.point(*v_it);
        if (coord[0] < min_x) min_x = coord[0];
        if (coord[0] > max_x) max_x = coord[0];
        if (coord[1] < min_y) min_y = coord[1];
        if (coord[1] > max_y) max_y = coord[1];
        if (coord[2] < min_z) min_z = coord[2];
        if (coord[2] > max_z) max_z = coord[2];
        size ++;
    }

    qDebug() << "coord min : " << min_x << min_y << min_z;
    qDebug() << "coord max : " << max_x << max_y << max_z;
    qDebug() << "this->offset : " << this->offset[0] << this->offset[1] << this->offset[2];
    qDebug() << "offset : " << offset[0] << offset[1] << offset[2];
    qDebug() << "off setted : ";
    qDebug() << "coord min : " << min_x + offset[0] << min_y + offset[1]<< min_z + offset[2];
    qDebug() << "coord max : " << max_x + offset[0]<< max_y + offset[1]<< max_z + offset[2];
    /*
    qDebug() << "taile du mesh : " << size;

    QVector3D box_size(max_x - min_x, max_y - min_y, max_z - min_z);
    QVector3D center((min_x + max_x)/2, (min_y + max_y)/2, (min_z + max_z)/2);
    */
    min[0] = min_x;
    min[1] = min_y;
    min[2] = min_z;

    max[0] = max_x;
    max[1] = max_y;
    max[2] = max_z;

    min += offset;
    max += offset;
}

BoundBox::BoundBox() {
    min[0] = 0.0;
    min[1] = 0.0;
    min[2] = 0.0;

    max[0] = 0.0;
    max[1] = 0.0;
    max[2] = 0.0;

}

bool BoundBox::contains(MyMesh mesh, MyMesh::Point offset) {
    for(MyMesh::FaceIter curFace = mesh.faces_begin(); curFace != mesh.faces_end(); curFace++) {
        for (MyMesh::FaceVertexIter curVert = mesh.fv_iter(*curFace); curVert.is_valid(); curVert++) {
            MyMesh::Point point = mesh.point(*curVert) + offset;
            if(!this->contains(point)) {
                return false;
            }
        }
    }
    return true;
}

bool BoundBox::contains(BoundBox &other) {
    return min[0] <= other.min[0] && max[0] >= other.max[0] &&
            min[1] <= other.min[1] && max[1] >= other.max[1] &&
            min[2] <= other.min[2] && max[2] >= other.max[2];
}

bool BoundBox::contains_strictly(BoundBox &other) {
    return min[0] <= other.min[0] && max[0] >= other.max[0] &&
            min[1] <= other.min[1] && max[1] >= other.max[1] &&
            min[2] <= other.min[2] && max[2] >= other.max[2];
}

bool BoundBox::contains(MyMesh::Point &pt) {
    return  min[0] <= pt[0] && pt[0] <= max[0] &&
            min[1] <= pt[1] && pt[1] <= max[1] &&
            min[2] <= pt[2] && pt[2] <= max[2];
}

bool BoundBox::contains(MyMesh mesh, MyMesh::Point offset, FaceHandle f) {
    //MyMesh::Point acc = MyMesh::Point(0,0,0);
    //pour chaque sommet
    for(MyMesh::FaceVertexIter fv_it = mesh.fv_iter(f); fv_it.is_valid(); fv_it ++) {
        MyMesh::Point pt = mesh.point(*fv_it) + offset;
        //acc += pt;
        if(contains(pt)) {
            return true;
        }
    }/*
    acc /= 3;
    if(contains(acc)) {
        return true;
    }*/
    return false;
}

float BoundBox::volume() {
    MyMesh::Point dim = max - min;
    return (dim[0] * dim[1] * dim[2]);
}

//helper : en fonction de la profondeur d'un octree renvoi la plus petite dimension possible
float BoundBox::min_volume(int depth) {
    float vol = volume();
    return vol/(8 * (depth - 1));
}

bool BoundBox::overlap(BoundBox& other, BoundBox* merge) {
    //Si les deux BoundBox sont confondues on renvoie l'une ou l'autre
    if(contains(other)) {
        *merge = other;
        return true;
    }
    else if(other.contains(*this)){
        *merge = *this;
        return true;
    }

    //Si elles ne sont pas confondues du tout
    if(max[0] < other.min[0] || min[0] > other.max[0]
            || max[1] < other.min[1] || min[1] > other.max[1]
            || max[2] < other.min[2] || min[2] > other.max[2]){
        *merge = BoundBox();
        return false;
    }

    //dans les autres cas, on va creer l'union des BoundBox
    merge->min[0] = std::min(min[0], other.min[0]);
    merge->min[1] = std::min(min[1], other.min[1]);
    merge->min[2] = std::min(min[2], other.min[2]);

    merge->max[0] = std::max(max[0], other.max[0]);
    merge->max[1] = std::max(max[1], other.max[1]);
    merge->max[2] = std::max(max[2], other.max[2]);
    return true;
}

BoundBox parts(MyMesh::Point max, MyMesh::Point min) {
    BoundBox res;
    res.min = min;
    res.max = max;

    //qDebug() << "coord min : " << res.min.to_string();
    //qDebug() << "coord max : " << res.max.to_string();

    return res;
}

std::array<BoundBox, 8> BoundBox::partition() {
    //qDebug() << "Test : min.x : " << min.x;
    MyMesh::Point center((min[0] + max[0])/2,(min[1] + max[1])/2,(min[2] + max[2])/2);

    BoundBox box1 = parts(MyMesh::Point(center[0], center[1], center[2]), MyMesh::Point(min[0], min[1], min[2]));
    BoundBox box2 = parts(MyMesh::Point(max[0], center[1], center[2]), MyMesh::Point(center[0], min[1], min[2]));
    BoundBox box3 = parts(MyMesh::Point(center[0], max[1], center[2]), MyMesh::Point(min[0], center[1], min[2]));
    BoundBox box4 = parts(MyMesh::Point(max[0], max[1], center[2]), MyMesh::Point(center[0], center[1], min[2]));
    BoundBox box5 = parts(MyMesh::Point(center[0], center[1], max[2]), MyMesh::Point(min[0], min[1], center[2]));
    BoundBox box6 = parts(MyMesh::Point(max[0], center[1], max[2]), MyMesh::Point(center[0], min[1], center[2]));
    BoundBox box7 = parts(MyMesh::Point(center[0], max[1], max[2]), MyMesh::Point(min[0], center[1], center[2]));
    BoundBox box8 = parts(MyMesh::Point(max[0], max[1], max[2]), MyMesh::Point(center[0], center[1], center[2]));

    std::array<BoundBox, 8> parts = {
        box1, box2, box3, box4, box5, box6, box7, box8
    };
    return parts;
}
