#ifndef POINT3D_H
#define POINT3D_H

#include <QString>
#include "mymesh.h"

class Point3D
{
public:
    Point3D();
    Point3D(float x, float y, float z);

    //std::string to_string();
    QString to_string();

    float x, y, z;

    Point3D operator+(const Point3D& p) {
        Point3D res;
        res.x = x + p.x;
        res.y = y + p.y;
        res.z = z + p.z;
        return res;
    }
    Point3D operator-(const Point3D& p) {
        Point3D res;
        res.x = std::abs(x - p.x);
        res.y = y - p.y;
        res.z = z - p.z;
        return res;
    }
    bool operator==(const Point3D& p) {
        return (x - p.x + y - p.y + z - p.z) == 0.0f;
    }
    bool operator!=(const Point3D& p) {
        return !operator==(p);
    }
};

#endif // POINT3D_H
