#include "point3d.h"

Point3D::Point3D()
{
    this->x = 0.0f;
    this->y = 0.0f;
    this->z = 0.0f;
}

Point3D::Point3D(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

QString Point3D::to_string() {
    QString str = QString("( %1, %2, %3)").arg(x).arg(y).arg(z);
    return str;
}

