#ifndef MOVABLEMESH_H
#define MOVABLEMESH_H

#include "mymesh.h"
#include <QVector>
#include <QMap>
#include <QSet>
#include "projection.h"
#include "polygone.h"

typedef std::set<MyMesh::Point>::iterator SETPOI;


//Nécéssaire pour trier le set
struct MMPointCompare{
    bool operator()(const SETPOI& p1, const SETPOI& p2)
    {
        if((*p1)[0] != (*p2)[0])return (*p1)[0] < (*p2)[0];
        if((*p1)[1] != (*p2)[1])return (*p1)[1] < (*p2)[1];
        return (*p1)[2] < (*p2)[2];
    }

};
using VFace = std::array<VertexHandle, 3>;

class MovableMesh
{
    MyMesh::Point deplacement;
    //Toutes les intersections pour la face int
    std::set<MyMesh::Point> intersection_points;
    //Permet de garder les accésseurs au vertexHandle de mesh nécéssaire lors de la retriangulation
    std::map<SETPOI, VertexHandle, MMPointCompare> it_pts;
    QVector<Edge> * intersections;
    void Init();
    void addIntersetion(int faceIdx, QPair<MyMesh::Point, MyMesh::Point> pair);
    int lineThickness=1;
public:
    MyMesh mesh;

    MovableMesh(MyMesh _mesh);
    MovableMesh(){}
    ~MovableMesh(){}
    void concatToMesh(MyMesh &_m);
    void displace(MyMesh::Point displacement);
    friend bool operator==(const MovableMesh& m1, const MovableMesh& m2);
    MyMesh getMesh();
    MyMesh::Point getOffset() {
        return deplacement;
    }
    MovableMesh* intersectFaceWithFace(QVector<FaceHandle> faces1, MovableMesh &mesh2, QVector<FaceHandle> faces2);
    MovableMesh *intersectWith(MovableMesh * mesh2);
    QVector<SETPOI> findNeighbor(int facecID, SETPOI n);
    QVector<Edge> findNeighborEdges(int facecID, SETPOI n);
    //À utiliser après avoir intersecter avec tous les autres mesh.
    void generateLoop();
    void find_border_vertex();
    void add_border_edge(int faceId);
    std::vector<Polygone> genLoop(int faceID);
    QVector<VFace> triangulateOperation(int faceID);
    QPair<std::vector<Polygone>, std::vector<Polygone> > find_all_polygon(int faceID);
    std::vector<std::vector<SETPOI>> find_cycle(int faceID, SETPOI current, SETPOI pre, std::vector<SETPOI> &already_visited);
};

#endif // MOVABLEMESH_H
