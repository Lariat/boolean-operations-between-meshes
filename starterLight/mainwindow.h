#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QListWidget>
#include <QVector>
#include <QFileDialog>
#include <QMainWindow>

#include "mymesh.h"
#include "movablemesh.h"
#include "octtree.h"
#include "projection.h"


namespace Ui {
class MainWindow;
}

using namespace OpenMesh;
using namespace OpenMesh::Attributes;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    float of = 0.01f;
    float volume = 0.0f;
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void displayMeshes();
    void displayMesh(MyMesh *_mesh, bool isTemperatureMap = false, float mapRange = -1);
    void resetAllColorsAndThickness(MyMesh* _mesh);
    bool RayIntersectsTriangle(QVector3D rayOrigin, QVector3D rayVector, FaceHandle inTriangle, QVector3D& outIntersectionPoint);
    void Dodavork_intersection();

    QVector<MovableMesh*> get_active_mesh();
    QVector<std::pair<MovableMesh*, int>> get_active_mesh_plus_index();
    void addMesh();
    void addMesh(QString filename, MyMesh meshh);
    void removeMesh();
    void deplacementMesh(MyMesh::Point deplacement);
    std::pair<QVector<FaceHandle>, QVector<FaceHandle>> find_smallest_region(MovableMesh m_1, MovableMesh m_2, OctTree m_1t, OctTree m_2t);

private slots:
    void on_spinBox_volume_valueChanged(int v);
    void on_spinBox_spd_valueChanged(int v);
    void on_pushButton_chargement1_clicked(){addMesh();}
    void on_addButton_clicked(){addMesh();}
    void on_removeButton_clicked(){removeMesh();}
    void on_pushButton_oct_clicked();
    void on_pushButton_doda_clicked();
    void on_pushButton_Rintersect_clicked();
    void on_pushButton_Test_clicked();

    void on_pushButton_pX_clicked(){deplacementMesh(MyMesh::Point(+of,0,0));}
    void on_pushButton_pY_clicked(){deplacementMesh(MyMesh::Point(0,+of,0));}
    void on_pushButton_pZ_clicked(){deplacementMesh(MyMesh::Point(0,0,+of));}
    void on_pushButton_mX_clicked(){deplacementMesh(MyMesh::Point(-of,0,0));}
    void on_pushButton_mY_clicked(){deplacementMesh(MyMesh::Point(0,-of,0));}
    void on_pushButton_mZ_clicked(){deplacementMesh(MyMesh::Point(0,0,-of));}
    void on_pushButton_tdi_clicked();
    void on_pushButton_tdio_clicked();

private:

    MyMesh mesh1;
    MyMesh mesh2;
    bool mesh_actif = false;
    QVector<QPair<QListWidgetItem*, MovableMesh*>> meshList;
    QVector<OctTree> octreeList;

    typedef struct{
        MovableMesh* mesh;
        QVector<QPair<MyMesh::Point, MyMesh::Point>> intersections;
    }IntersectionsOfMesh;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
