#ifndef OCTTREE_H
#define OCTTREE_H
#include <QList>
#include <QQueue>
#include "boundbox.h"

/*
 * Criteres importants :
 * -on s arrête si on trouve qu 'un seul elt
 * -on s arrête si on a atteint des boites trop petites (ie cube 1*1*1)
 *  |_ dans ce cas check de collision brute
 * -Si une region contenante n'a en fait pas d objet elle sert à rien dans l'Octree
 *
 * Notre Octree ne checkera que des regions cubiques et pas spheriques
 *
 * Chaque Noeud a une region limite qui définit la bordure
 * Chaque noeud a une ref vers le noeud parent
 * Chaque noeud a un tableau contenant 8 fils
 * Chaque noeud a une liste des objets contenus dans sa région
*/

class OctTree
{
public:
    //constructeurs
    OctTree();
    OctTree(BoundBox region);
    OctTree(BoundBox box, MyMesh test, int id);

    //lien vers le noeud parent
    OctTree * parent = nullptr;

    //region(géométrique) du noeud
    BoundBox region;

    //lien vers les noeuds fils
    std::vector<OctTree> children = std::vector<OctTree>();

    //Liste des objets appartenant à la region
    //ici nos objets sont des listes de vertexHandle qui représentent des faces
    //on s'attend à avoir que des faces triangulaires ainsi :
    //une liste de suite de trois vertexHandle
    //QList<std::vector<MyMesh::VertexHandle>> objects;


    //On va stocker des liens vers les vertex Handle
    QVector<MyMesh::FaceHandle> mesh_face;

    //Queue pour l'insertion d'elt dans la liste
    QQueue<std::vector<MyMesh::VertexHandle>> insertion = QQueue<std::vector<MyMesh::VertexHandle>>();


    int ID = 0; //la racine
    int depth = -1;
    bool initialiased;
    bool is_terminal = false;

    //Taille min pour une region -> voir + petit
    int MIN_SIZE = 1;

    bool tree_ready = false;
    bool tree_built = false;

    void debug();
    void updateTree();
    void buildTree(MyMesh &mesh, MyMesh::Point offset, QVector<MyMesh::FaceHandle> faces,
                   int &depth, float volume, int cur_depth = 0);
    bool insert(std::vector<MyMesh::VertexHandle>);
    void draw(MyMesh &_mesh, int dDepth, MyMesh::Point offset, MyMesh::Color col = MyMesh::Color(255, 0, 0));
    //partition en 8 de l'espace -> 1 sous-mesh par partie
    //OctTree createNode(BoundBox region, MyMesh test, std::vector<MyMesh> objList);
    //OctTree createNode(BoundBox region, MyMesh obj);

    void Octree_as_array(OctTree cur, std::vector<OctTree> &res);
    void Octree_as_sub_array(int depth, OctTree cur, std::vector<OctTree> &res);
    bool isNull() {
        return initialiased;
    }
};

#endif // OCTTREE_H
