#include "tri_intersection_tools.h"
#include "memory.h"

// Ce qui est implémenté ici sont les opérations 1 et 2
// issues du papier "A Fast Triangle-Triangle Intersection Test"
// de Tomas Möller 1997

auto getTrianglePoints( MyMesh _mesh, MyMesh::FaceHandle fh ) -> std::vector<MyMesh::Point>
{
    //
    std::vector<MyMesh::Point> p;

    MyMesh::ConstFaceVertexRange fv_range = _mesh.fv_range( fh );

    for ( MyMesh::FaceVertexIter fv_it = std::begin( fv_range ), it_end = std::end( fv_range );
          fv_it != it_end; ++fv_it )
    {
        p.push_back( _mesh.point( *fv_it ) );
    }
    return p;
}

// Supposément le calcul de la normale d'une face d'un triangle
auto computeN( std::vector<MyMesh::Point> points ) -> MyMesh::Point
{
    return ( ( points.at( 1 ) - points.at( 0 ) ) % ( points.at( 2 ) - points.at( 0 ) ) );
}

auto computeD( MyMesh::Point N, MyMesh::Point V ) -> float
{
    return ( -std::move( N ) | V );
}

auto getSign( float val ) -> int
{
    if ( val > 0 )
        return 1;
    return 0;
}

auto isIntersection( MyMesh _mesh, MyMesh::FaceHandle fh1, MyMesh::FaceHandle fh2 ) -> bool
{
    std::vector<MyMesh::Point> points1 = getTrianglePoints( _mesh, fh1 );
    std::vector<MyMesh::Point> points2 = getTrianglePoints( _mesh, fh2 );
    float distance1[ 3 ];
    float distance2[ 3 ];

    MyMesh::Point N1 = computeN( points1 ), N2 = computeN( points2 );
    float D1 = computeD( N1, points1.at( 0 ) ), D2 = computeD( N2, points2.at( 0 ) );

    int index = 0;

    // compute plan π1
    for ( auto tri_it = points1.begin(); tri_it != points1.end(); ++tri_it )
    {
        distance1[ index ] = ( ( N2 | *tri_it ) + D2 );
        index++;
    }

    index = 0;

    // compute plan π2
    for ( auto tri_it = points2.begin(); tri_it != points2.end(); ++tri_it )
    {
        distance2[ index ] = ( ( N1 | *tri_it ) + D1 );
        index++;
    }

    bool diff_sign1 = true, diff_sign2 = true;
    // bool coplanar = false;
    float cntrl1 = 0.f, cntrl2 = 0.f;

    for ( int i = 0; i < 3; i++ )
    {
        cntrl1 += distance1[ i ];
        cntrl2 += distance2[ i ];
    }

    if ( cntrl1 == 0.f && cntrl2 == 0.f )
        return false;

    if ( getSign( distance1[ 0 ] ) && getSign( distance1[ 1 ] ) && getSign( distance1[ 2 ] ) )
        diff_sign1 = false;

    if ( getSign( distance2[ 0 ] ) && getSign( distance2[ 1 ] ) && getSign( distance2[ 2 ] ) )
        diff_sign2 = false;

    if ( diff_sign1 == true && diff_sign2 == true )
        return true;

    return false;
}
