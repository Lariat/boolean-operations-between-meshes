#ifndef CYCLE_H
#define CYCLE_H

#include "mymesh.h"
#include "projection.h"
#include <QVector>
#include <QQueue>
using namespace std;

struct MMPointCompare{
    bool operator()(const SETPOI& p1, const SETPOI& p2)
    {
        if((*p1)[0] != (*p2)[0])return (*p1)[0] < (*p2)[0];
        if((*p1)[1] != (*p2)[1])return (*p1)[1] < (*p2)[1];
        return (*p1)[2] < (*p2)[2];
    }

};

class Cycle{

    map<SETPOI, QVector<SETPOI>, MMPointCompare> adjacency;
    int ** level;

    int positionOf(SETPOI s){return std::distance(adjacency.begin(), adjacency.find(s));}

    QVector<SETPOI> findNeighbor(SETPOI v, QVector<Edge> edges){
        QVector<SETPOI> neighs;
        for(Edge p : edges){
            if(p.A == v || p.B == v){
                neighs.push_back((p.A==v)?p.B:p.A);
            }
        }
        return neighs;
    }

    Cycle(QVector<Edge> _edges){
        for(Edge e:_edges){
            adjacency.insert({e.A, QVector<SETPOI>()});
            adjacency.insert({e.B, QVector<SETPOI>()});
        }
        for(pair<SETPOI,QVector<SETPOI>> s : adjacency){
            s.second = findNeighbor(s.first, _edges);
        }
        level = new int*[adjacency.size()];
        for(int i=0; i< adjacency.size(); i++){
            level[i] = new int[adjacency.size()];
            for(int j=0; j<adjacency.size(); j++){
                level[i][j] = 0;
            }
        }
    }

    int& lv(SETPOI u, SETPOI v){return level[positionOf(u)][positionOf(v)];}

    void front(SETPOI v){
        for(pair<SETPOI,QVector<SETPOI>> s : adjacency){
            QQueue<SETPOI> q; lv(v,v)=0; q.enqueue(s.first);
            while(!q.isEmpty()){
                SETPOI u = q.dequeue();
                for(SETPOI w : adjacency.find(u)->second){
                    if(lv(v,w) == -1){
                        lv(v,w) = lv(v,u)+1;
                        q.enqueue(w);
                    }else if(lv(v,u) < lv(v,w)){

                        return ;
                    }
                }
            }
        }
    }
    void edge();
    void min_circuit();
};

#endif // CYCLE_H
