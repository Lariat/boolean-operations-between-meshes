#include "meshmeshroughintersection.h"

void MeshMeshRoughIntersection::get_intersection_region(OctTree oct1, OctTree oct2, std::vector<OctTree> &res) {

    BoundBox * a = new BoundBox() ;
    if(oct1.region.overlap(oct2.region, a) == false) return;

    //IL y a possibilité d'intersection : on affine le résultat :
    for(size_t i = 0; i < 8; i++) {
        for(size_t j = 0; j < 8; j++) {
            //Si nos noeuds fils sont initialisés
            if(oct1.children.at(i).isNull() && oct2.children.at(j).isNull()) {
                if(oct1.children.at(i).region.overlap(oct2.children.at(j).region, a)) {
                    res.push_back(oct1.children.at(i));
                    res.push_back(oct2.children.at(j));
                    get_intersection_region(oct1.children.at(i), oct2.children.at(j), res);
                }
            }
        }
    }
}

