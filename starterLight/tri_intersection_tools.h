#ifndef TRI_INTERSECTION_TOOLS_H
#define TRI_INTERSECTION_TOOLS_H

#include "mymesh.h"
#include <QDebug>

auto getTrianglePoints( MyMesh _mesh, MyMesh::FaceHandle fh ) -> std::vector<MyMesh::Point>;

auto computeN( std::vector<MyMesh::Point> points ) -> MyMesh::Point;

auto computeD( MyMesh::Point N, MyMesh::Point V ) -> float;

auto isIntersection( MyMesh _mesh, MyMesh::FaceHandle fh1, MyMesh::FaceHandle fh2 ) -> bool;

#endif // TRI_INTERSECTION_TOOLS_H
