#include "octtree.h"

// ______________ Constructeurs ______________

OctTree::OctTree()
{
    //Peut-être initialisé une bound box de taile 0x0x0
    region = BoundBox();
    debug();
}


//Might depreciate all the others Constructors...
OctTree::OctTree(BoundBox box, MyMesh test, int id) {
    region = box;
    ID = id;
}

OctTree::OctTree(BoundBox box) {
    region = box;
    initialiased = false;
}

void OctTree::debug() {
    //A compléter si besoin !
}

// ______________ Méthodes ______________

void OctTree::updateTree() {
    /*
    if(!tree_built) {
        while(!insertion.isEmpty())
            objects.push_back(insertion.dequeue());
        buildTree();
    }
    else {
        while(!insertion.isEmpty())
            insert(insertion.dequeue());
    }
    tree_ready = true;
    */
}

void OctTree::buildTree(MyMesh &mesh, MyMesh::Point offset, QVector<MyMesh::FaceHandle> faces, int &depth, float volume, int cur_depth) {
    //valeur a changer ?
    if(cur_depth == depth) {
        is_terminal = true;
        return;
    }
    //si on a plus assez d'éléments -> fin recursion
    if(faces.size() <= 0) {
        //qDebug() << "faces est trop petit on coupe";
        is_terminal = true;
        return;
    }
    //si region trop petite -> fin recursion
    if(region.volume() < volume) {
        qDebug() << "REGION.VOL : " << region.volume();
        qDebug() << "VOL : " << volume;
        depth = cur_depth;
        is_terminal = true;
        return;
    }

    initialiased = true;
    //on remplit le sous-mesh du noeud courant
    //qDebug() << "nb faces :" << faces.size();
    MyMesh::Color col(rand() % 255, rand() % 255, rand() % 255);
    for(FaceHandle f : faces) {
        if(region.contains(mesh, offset, f)) {
            mesh_face.push_back(f);
            mesh.set_color(f, col);
        }
    }

    //qDebug() << "test :" << mesh_face.size();

    //on split la région en 8 si elle passe les tests précédents
    std::array<BoundBox, 8> octant = region.partition();
    this->depth = cur_depth;
    cur_depth ++;
    //qDebug() << depth;
    //creation des fils
    for(size_t i = 0; i < 8; i ++) {
        children.push_back(OctTree(octant[i]));
        //children.at(i).ID += i + 1;
        children.at(i).parent = this;
        children.at(i).buildTree(mesh, offset, mesh_face, depth, volume, cur_depth);
        //qDebug() << "node[" << depth * 8 + i<< "] : (" << children[i].region.min.to_string() << ", "<< children[i].region.max.to_string() << ")";
    }
}

void OctTree::Octree_as_array(OctTree cur, std::vector<OctTree> &res) {
    if(cur.initialiased == true) {
        res.push_back(cur);
        for(size_t i = 0; i < 8; i++) {
            Octree_as_array(cur.children.at(i), res);
        }
    }
}

void OctTree::Octree_as_sub_array(int depth, OctTree cur, std::vector<OctTree> &res) {
    if(cur.initialiased == true) {
        if(cur.depth == depth) {
            res.push_back(cur);
        }
        for(size_t i = 0; i < 8; i++) {
            Octree_as_sub_array(depth, cur.children.at(i), res);
        }
    }
}

void OctTree::draw(MyMesh &_mesh, int dDepth, MyMesh::Point offset, MyMesh::Color col) {
    if(initialiased) {
        if(depth == dDepth) {
            region.draw(_mesh, offset, col);
        }
        for(size_t i = 0; i < 8; i++) {
            if(children.at(i).is_terminal == false) {
                children.at(i).draw(_mesh, dDepth, offset, col);
            }
        }
    }else {
        return;
    }
}
/*
OctTree OctTree::createNode(BoundBox region, MyMesh test, std::vector<MyMesh> objList) {
    //Check si explose
    //if(objList.size() == 0) return OctTree();
    //OctTree ret = OctTree(region, test, objList);
    ret.parent = this;
    return ret;
}*/

/*
OctTree OctTree::createNode(BoundBox region, MyMesh obj) {
    //Check si explose
    OctTree ret = OctTree(region, obj);
    ret.parent = this;
    return ret;
}*/

bool OctTree::insert(std::vector<MyMesh::VertexHandle> obj) {
    /*
    if(objects.count() == 0 && active_nodes == 0) {
        objects.push_back(obj);
        return true;
    }
    Point3D dim(region.max.x - region.min.x, region.max.y - region.min.y, region.max.z - region.min.z);

    //peut etre verifier que la dimension n est pas nulle

    //verification que la dimension n est pas trop petite
    if(dim.x <= MIN_SIZE && dim.y <= MIN_SIZE && dim.z <= MIN_SIZE) {
        objects.push_back(obj);
        return true;
    }
    MyMesh tmp;
    tmp.add_face(obj);
    if(region.contains(tmp)) {
        //?Pas sur du tout
        if(this->parent != nullptr) return this->parent->insert(obj);
        else return false;
    }

    std::array<BoundBox, 8> child_octant = region.partition();
    for(int i = 0; i < 8; i++)
        child_octant[i] = (children[i] != nullptr) ? children[i].region : child_octant[i];

    if(region.contains(BoundBox(tmp))) {
        bool found = false;
        for(int i = 0; i< 8; i ++) {
            if(child_octant[i].contains(BoundBox(tmp))) {
                if(children[i] != nullptr) return children[i].push_back(obj);
                else {
                    children[i] = createNode(child_octant[i], obj);
                    active_nodes |= (1 << i);
                }
                found = true;
            }
        }
        if (!found) {
            objects.push_back(obj);
            return true;
        }
    }
    return false;
    */
    return true;
}
